﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotarSkybox : MonoBehaviour
{
    public float velocidadDeRotacion = 0f;

    void Update()
    {
        RenderSettings.skybox.SetFloat("_Rotation", Time.time * velocidadDeRotacion);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorCamino : MonoBehaviour
{
    [SerializeField] private GameObject contenedorCaminosGO;
    [SerializeField] private GameObject[] contenedorCaminosArray;
    [SerializeField] private GameObject caminoAnterior;
    [SerializeField] private GameObject caminoNuevo;
    [SerializeField] private GameObject mainCamaraGO;
    [SerializeField] private Camera mainCamera;
    [SerializeField] private float velocidad = 0f;
    [SerializeField] private Vector3 medidaLimitePantalla;

    private int contadorCaminos = 0;
    private int selectorCamino = 0;
    private float tamañoCamino;
    private bool salioDeLaPantalla = true;
    private bool inicioJuego = true;
    private bool FinDeJuego = false;

    private void Start()
    {
        InicioJuego();
    }
    private void InicioJuego()
    {
        contenedorCaminosGO = GameObject.Find("ContenedorCaminos");
        mainCamaraGO = GameObject.Find("MainCamera");
        mainCamera = mainCamaraGO.GetComponent<Camera>();
        
        VelocidadMotorCamino();

        BuscoCaminos();
    }
    private void VelocidadMotorCamino()
    {
        velocidad = velocidad + 11.50f;
    }
    private void BuscoCaminos()
    {
        contenedorCaminosArray = GameObject.FindGameObjectsWithTag("Camino");
        for (int posicion = 0; posicion < contenedorCaminosArray.Length; posicion++)
        {
            contenedorCaminosArray[posicion].gameObject.transform.parent = contenedorCaminosGO.transform;
            contenedorCaminosArray[posicion].gameObject.SetActive(false);
            contenedorCaminosArray[posicion].gameObject.name = "CaminoOff_" + posicion; 
        }
        CrearCaminos();
    }
    private void CrearCaminos()
    {
        contadorCaminos = contadorCaminos + 1;
        selectorCamino = Random.Range(0, contenedorCaminosArray.Length);
        GameObject camino = Instantiate(contenedorCaminosArray[selectorCamino]);
        camino.SetActive(true);
        camino.name = "Camino" + contadorCaminos;
        camino.transform.parent = gameObject.transform;
        PosicionoCaminos();
    }
    private void PosicionoCaminos()
    {
        caminoAnterior = GameObject.Find("Camino" + (contadorCaminos - 1));
        caminoNuevo = GameObject.Find("Camino" + contadorCaminos);
        MidoCaminos();
        caminoNuevo.transform.position = new Vector3(caminoAnterior.transform.position.x, caminoAnterior.transform.position.y,caminoAnterior.transform.position.z + tamañoCamino);
        salioDeLaPantalla = false;
    }
    private void MidoCaminos()
    {
        for (int contador = 0; contador < caminoAnterior.transform.childCount; contador++)
        {
            if (caminoAnterior.transform.GetChild(contador).gameObject.GetComponent<Pieza>() != null)
            {
                float tamañaoPieza = caminoAnterior.transform.GetChild(contador).gameObject.GetComponent<MeshRenderer>().bounds.size.z;
                tamañoCamino = tamañoCamino + tamañaoPieza;    
            }
        }
    }
    private void MedirPantalla()
    {
        medidaLimitePantalla = new Vector3(0, 0, mainCamera.ScreenToWorldPoint(new Vector3(0, 0, 0)).z - 0.5f);
    }
    private void Update()
    {
        if (inicioJuego == true && FinDeJuego == false)
        {
            this.transform.Translate(Vector3.back * velocidad * Time.deltaTime);

            if (caminoAnterior.transform.position.z + tamañoCamino < medidaLimitePantalla.z && salioDeLaPantalla == false)
            {
                salioDeLaPantalla = true;
                DestruirCamino();
            }
        }
    }
    private void DestruirCamino()
    {
        Destroy(caminoAnterior);
        tamañoCamino = 0;
        caminoAnterior = null;
        CrearCaminos();
    }
}

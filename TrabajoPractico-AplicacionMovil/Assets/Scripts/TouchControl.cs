﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControl : MonoBehaviour
{
    [SerializeField] private GameObject jugadorGO;

    private Vector3 posicionActualJugador;
    private GameObject posicionIzquierdaGO;
    private GameObject posicionMedioGO;
    private GameObject posicionDerechaGO;

    private float fuerzaDeSalto = 4.5f;

    private Touch toque;
    private Vector2 primerPosicionToque;

    [SerializeField] float minSwipeX;
    [SerializeField] float minSwipeY;

    private Animator animacionChicho;

    private bool puedoSaltar = true;
    private bool puedoVibrar = false;

    void Start()
    {
        jugadorGO = GameObject.FindObjectOfType<Jugador>().gameObject;
        posicionIzquierdaGO = GameObject.Find("PosicionIzquierda");
        posicionMedioGO = GameObject.Find("PosicionMedio");
        posicionDerechaGO = GameObject.Find("PosicionDerecha");
        posicionActualJugador = jugadorGO.transform.position;
        animacionChicho = jugadorGO.GetComponent<Animator>();

    }
    void Update()
    {
        Toques();
    }
    private void Toques()
    {
        if (Input.touchCount > 0)
        {
            toque = Input.touches[0];

            if (toque.phase == TouchPhase.Began)
            {
                primerPosicionToque = toque.position;
            }
            else if (toque.phase == TouchPhase.Ended)
            {
                float swipeHorizontal = (new Vector2(toque.position.x, 0f) - (new Vector2(primerPosicionToque.x, 0f))).magnitude;
                float swipeVertical = (new Vector2(0f, toque.position.y) - (new Vector2(0f, primerPosicionToque.y))).magnitude;
                //Se reliza Swipe tanto para la izquierda como la derecha.
                if (swipeHorizontal > minSwipeX)
                {
                    float swipeValor = Mathf.Sign(toque.position.x - primerPosicionToque.x);
                    if (swipeValor > 0)
                    {
                        if (jugadorGO.transform.position.x == posicionMedioGO.transform.position.x || jugadorGO.transform.position.x == posicionIzquierdaGO.transform.position.x)
                        {
                            posicionActualJugador.x = -0.67f;
                            jugadorGO.transform.position -= posicionActualJugador;
                        }
                    }
                    else if (swipeValor < 0)
                    {
                        if (jugadorGO.transform.position.x == posicionMedioGO.transform.position.x || jugadorGO.transform.position.x == posicionDerechaGO.transform.position.x)
                        {
                            posicionActualJugador.x = 0.67f;
                            jugadorGO.transform.position -= posicionActualJugador;
                        }
                    }
                }
                //Se realiza Swipe tanto arriba como abajo.
                if (swipeVertical > minSwipeY)
                {
                    float swipeValor = Mathf.Sign(toque.position.y - primerPosicionToque.y);
                    if (swipeValor > 0)
                    {
                        if (puedoSaltar == true)
                        {
                            jugadorGO.GetComponent<Rigidbody>().AddForce(new Vector3(0f, fuerzaDeSalto, 0f), ForceMode.Impulse);
                            animacionChicho.SetTrigger("Salto");
                            puedoSaltar = false;
                            puedoVibrar = true;
                        }
                      
                    }
                    else if (swipeValor < 0)
                    {
                        animacionChicho.SetTrigger("Roll");
                    }
                }
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        puedoSaltar = true;
        if (puedoVibrar == true)
        {
            Handheld.Vibrate();
            puedoVibrar = false;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teclado : MonoBehaviour
{
    [SerializeField] private GameObject jugadorGO;

    private Vector3 posicionActualJugador;
    private GameObject posicionIzquierdaGO;
    private GameObject posicionMedioGO;
    private GameObject posicionDerechaGO;

    private float fuerzaDeSalto = 4.5f;

    private Animator animacionChicho;
    public bool puedoSaltar = true;
    private void Start()
    {
        jugadorGO = GameObject.FindObjectOfType<Jugador>().gameObject;
        posicionIzquierdaGO = GameObject.Find("PosicionIzquierda");
        posicionMedioGO = GameObject.Find("PosicionMedio");
        posicionDerechaGO = GameObject.Find("PosicionDerecha");
        posicionActualJugador = jugadorGO.transform.position;
        animacionChicho = jugadorGO.GetComponent<Animator>();
    }
    private void Update()
    {      
        Teclas();
    }
    private void Teclas()
    {
       
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (jugadorGO.transform.position.x == posicionMedioGO.transform.position.x || jugadorGO.transform.position.x == posicionDerechaGO.transform.position.x)
            {
                posicionActualJugador.x = 0.67f;
                jugadorGO.transform.position -= posicionActualJugador;
            }  
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            if (jugadorGO.transform.position.x == posicionMedioGO.transform.position.x || jugadorGO.transform.position.x == posicionIzquierdaGO.transform.position.x)
            {
                posicionActualJugador.x = 0.67f;
                jugadorGO.transform.position += posicionActualJugador;
            }   
        }

        if (Input.GetKeyDown(KeyCode.W) && puedoSaltar == true)
        {
                jugadorGO.GetComponent<Rigidbody>().AddForce(new Vector3(0f, fuerzaDeSalto, 0f), ForceMode.Impulse);
                animacionChicho.SetTrigger("Salto");
                puedoSaltar = false;     
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            animacionChicho.SetTrigger("Roll");
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Suelo")
        {
            puedoSaltar = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class ScriptFondo : MonoBehaviour
{
    public Sprite[] Menu = new Sprite[3];
    const bool validar = true;
    private GameObject fondoGO;
    private Image fondoImage;
    void Start()
    {
        fondoGO = this.gameObject;
        fondoImage = fondoGO.GetComponent<Image>();
        StartCoroutine("CambiarFondo");
    }
    IEnumerator CambiarFondo()
    {
        while (validar == true){
            fondoImage.sprite = Menu[0];
            yield return new WaitForSeconds(2f);
            for (int i = 0; i <= 7; i++)
            {
                for (int j = 1; j <= 2; j++)
                {
                    this.gameObject.GetComponent<Image>().sprite = Menu[j];
                    yield return new WaitForSeconds(0.1f);
                }
            }
            
        }
    }
}

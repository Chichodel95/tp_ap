﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBoton : MonoBehaviour
{
    private float limiteArriba = 30f;
    private float limiteAbajo = -30f;
    private Vector3 angulo;
    private Vector3 testeando = new Vector3(0f,0f,30);
    private RectTransform esteRect;
    private Quaternion cuaternioAngulo;
    private Quaternion cuaternionRotacionPos = Quaternion.Euler(0f,0f, 30f);
    private Quaternion cuaternionRotacionNeg = Quaternion.Euler(0f, 0f, -30f);
    void Start()
    {
        esteRect = this.gameObject.GetComponent<RectTransform>();
    }
    void Update()
    { 
        RotarBoton();
    }
    private void RotarBoton( )
    {
        cuaternioAngulo = transform.rotation;
        angulo = cuaternioAngulo.eulerAngles;
        angulo += new Vector3(0f, 0f, 10f) * Time.deltaTime;
        cuaternioAngulo.eulerAngles = angulo;
        transform.rotation = cuaternioAngulo;

        if (esteRect.rotation.eulerAngles.z > angulo.z)
        {
            angulo += new Vector3(0f,0f,-10);
        }
        //else if (angulo.z <= limiteAbajo)
        //{
        //    angulo.z = 1;
        //}
        Debug.Log(esteRect.rotation.eulerAngles);
    }
}
